import React, { useContext, useState } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBContainer, MDBView, MDBMask } from 'mdbreact';
//import { BrowserRouter as Router } from 'react-router-dom';
import UserContext from '../UserContext';
import Register from '../pages/register';
import Login from '../pages/login';

//const { user } = useContext(UserContext);
  
 

class NavBar extends React.Component {



  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false,

    };
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.setState({
      collapse: !this.state.collapse,
    });
  }



  render() {



    return (
      <div>
    
            <MDBNavbar color="black" fixed="top" dark expand="md">
              <MDBContainer>
                <MDBNavbarBrand href="/">
                  <strong></strong>
                 </MDBNavbarBrand>
                  <MDBNavbarToggler onClick={this.onClick} />
                   <MDBCollapse isOpen={this.state.collapse} navbar>
                     <MDBNavbarNav left>

                        <MDBNavItem active>
                          <MDBNavLink to="/">Home</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem active>
                          <MDBNavLink to="/profile">Profile</MDBNavLink>
                        </MDBNavItem>



                    </MDBNavbarNav>
                  </MDBCollapse>
               </MDBContainer>
            </MDBNavbar>

     



             
 
       
  

      </div>
    );
  }
}
export default NavBar;