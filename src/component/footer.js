import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

const FooterPage = () => {
  return (
    <MDBFooter color="black" className="font-small ">
      <MDBContainer fluid className="text-center text-md-left">
        <MDBRow>
          <MDBCol md="6">
            <h5 className="title">Content</h5>
            <p>
            <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright: <a href="#"> for portfolio purposes only</a>
        </MDBContainer>
            </p>
          </MDBCol>
          <MDBCol md="6">
            <h5 className="title">Links</h5>
            <ul>
              <li className="list-unstyled">
                <a href="#!">Budget Tracker</a>
              </li>
              <li className="list-unstyled">
                <a href="#!">Course Booking</a>
              </li>
            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
   
    </MDBFooter>
  );
}

export default FooterPage;