// import logo from './logo.svg';
import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import { UserProvider } from './UserContext';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Login from './pages/login';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import NavBar from './component/NavBar'
import Home from './pages/home'
import Profile from './pages/profile'
import Register from './pages/register'


function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      email: null,
      isAdmin: null
    })
  }

  return (
    <React.Fragment>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
 
          <NavBar />
          <Switch>
            <Route exact path="/" component={Home}/>
               <Route exact path="/profile" component={Profile}/>
            <Route exact path="/register" component={Register}/>
          
            <Route exact path="/login" component={Login} />
          

          </Switch>
        
   
      </Router>
    </UserProvider>
  </React.Fragment>
  );
}

export default App;
