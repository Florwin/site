
import React, {useState, useEffect  } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { MDBInput, MDBView, MDBMask  } from "mdbreact";

export default function Register (){
   
        const [email, setEmail] = useState('');
        const [fname, setFname] = useState('');
        const [lname, setLname] = useState('');
        const [password1, setPassword1] = useState('');
        const [password2, setPassword2] = useState('');
        const [isActive, setIsActive] = useState(false);
        const [willRedirect, setWillRedirect] = useState(false)

    function registerUser(e) {
        e.preventDefault();
  
        setFname('');
        setLname('');
        setEmail('');
        setPassword1('');
        setPassword2('');
        console.log('Registered User')
        setWillRedirect(true);
    }

    useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
            console.log("log");
          
		}else{
			setIsActive(false);
  
		}

	},[email, password1, password2  ])

    return (

            willRedirect === true
            ?
            <Redirect to='/login' />
            :
            <MDBView src="https://res.cloudinary.com/florwinapp/image/upload/v1617546008/container1_oqppdn.jpg">
                <MDBMask overlay="black-strong" className="flex-center flex-column text-white text-center">
           
            <Form onSubmit = {e => registerUser(e)}>
                 
                <Form.Group controlId="firstName" >
                
                <MDBInput 
                    hint="First Name" 
                    type="name" 
                    value={fname}
                    onChange={e => setFname(e.target.value)}
                    size ="lg"   
                    autoFocus
                    placeholder="ex. Florwin"
                    required
                    />
                       
                </Form.Group>

                <Form.Group controlId="lastName" >
                
                <MDBInput 
                    hint="Last Name" 
                    type="name" 
                    value={lname}
                    onChange={e => setLname(e.target.value)}
                    size ="lg"   
                    autoFocus
                    placeholder="ex. Abayon"
                    required
                    />
                       
                </Form.Group>



                <Form.Group controlId="email" >
                <MDBInput 
                    hint="Your e-mail" 
                    type="email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    size ="lg"   
                    autoFocus
                    placeholder="ex. wowin@wowin.com"
                    required
                    />
                        <Form.Text className = "text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                </Form.Group>

                <Form.Group controlId="pw1">
                <MDBInput 
                    hint="Password" 
                    type="text" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    size ="lg"   
                    autoFocus
                    required
                    />
                </Form.Group>
                <MDBInput 
                    hint="Verify Password" 
                    type="text" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    size ="lg"   
                    autoFocus
                    required
                    />
                <Form.Group  controlId="pw2">
                  
                     <Form.Text className = "log-in">
                           Already Registered? Please Log in <a href="/login">here</a>.
                    </Form.Text>
                </Form.Group>

                { isActive                     
                        ?
                <Button className="bg-primary" type="submit" id="submitBtn"> Submit</Button>
                
                        :
                <Button className="bg-danger" type="submit" id="submitBtn" disabled> Submit </Button>
                    
                }
            </Form>
                       </MDBMask>
          </MDBView>
    )
}
