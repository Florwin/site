import React from 'react';
import { Col, Row } from 'react-bootstrap'
import NavBaR from '../../component/NavBar'
import { MDBInput, MDBView, MDBMask } from "mdbreact";
import FooterPage from '../../component/footer'
//import Animate from "../../styles/index"


export default function Home(){
    return (
<React.Fragment>

       <MDBView hover zoom src="https://res.cloudinary.com/florwinapp/image/upload/v1617546008/container1_oqppdn.jpg">
            <MDBMask overlay="pink-light" className="text1 flex-center flex-column text-white text-center fluid">
              <h2>FLORWIN ABAYON</h2>
              <h5>"My goal is to help my clients and achieve excellent work at all times."</h5>
              <br />
              <h4>Web Development and Web Design</h4>
  			</MDBMask>
     </MDBView>
    
       <MDBView hover zoom className="img1"> 
        
            <img 
            className="img-fluid"
            src="https://res.cloudinary.com/florwinapp/image/upload/v1617546008/container_xtcl1a.jpg"

            />
            <MDBMask overlay="pink-light" className="text2 flex-top flex-column text-white text-left">
              <Row>
                  <Col>
                      <p>
                         My journey of becoming a Web Developer and Web Design started last 2020, where I was fascinated by different Website Architectures during my work as a Researcher.
                         I began joining Trainings and Bootcamps to learn, as well as join groups to improve the use of these technologies and better equipped in the field of Multimedia. Other skills I've learned for the past 10 years include, Photoshop, Photography, Adobe lightroom, Basic Audition and Basic Video Editors. 
                      </p>
                  </Col>
              </Row>
            
            </MDBMask>
       </MDBView>
<FooterPage/>
</React.Fragment>

    );
}



