import React, { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import  UserContext   from '../../UserContext';
//import Users from '../../data/users';
import { MDBInput, MDBView, MDBMask } from "mdbreact";
//import { GoogleLogin } from 'react-google-login';
//import AppHelper from '../../app-helper';
//import Swal from 'sweetalert2';



function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [willRedirect, setWillRedirect] = useState(false)
    const [isActive, setIsActive] = useState(false)

   function authenticate(e) {
        e.preventDefault();
        
    //const { setUser } = useContext(UserContext);

 /*   const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [willRedirect, setWillRedirect] = useState(false)
    const [isActive, setIsActive] = useState(false)

    function authenticate(e) {
        e.preventDefault();
        
    const options = {
        method:'POST',
        headers: {'Content-Type': 'application/json'},
         body: JSON.stringify({
            email:email,
            password:password
         })
    }
    fetch(`https://localhost:3000/login`, options)

        .then(AppHelper.toJSON)
        .then(data =>{
        
        console.log(data)
        
        if(typeof data.accessToken !== 'undefined'){
            localStorage.setItem('token',data.accessToken)
            retrieveUserDetails(data.accessToken)
            console.log(localStorage)
        
        }else{
            if(data.error === 'does-not-exist'){
                Swal.fire('AuthenticationFailed','User does not Exist','error')
            }
            else if (data.error === 'login-type-error'){
                Swal.fire('Login Type Error','You may have registered through a different login procedure, try an alternative login procedure','error')
            }
            else if (data.error === 'incorrect-password'){
                Swal.fire('Authentication failed','Password is incorrect','error')
            }
        }
    })*/

       }
    return(
        willRedirect === true
        ?
        <Redirect to="/profile"/>
        :

<MDBView src="https://res.cloudinary.com/florwinapp/image/upload/v1617546008/container1_oqppdn.jpg">
    <MDBMask overlay="black-strong" className="flex-center flex-column text-white text-center">
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label></Form.Label>
                <MDBInput 
                    hint="Your e-mail" 
                    type="email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    size ="lg"   
                    autoFocus
                    placeholder="ex. wowin@wowin.com"
                    required
                    />
            </Form.Group>

            <Form.Group controlId="password">
                
                <MDBInput 
                    hint="Password" 
                    type="text" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    size ="lg"   
                    autoFocus
                    required
                    />
            </Form.Group>

            { isActive
                ? 
                <Button className="bg-primary" type="submit">
                    Submit
                </Button>
                :
                <Button className="bg-danger" type="submit" disabled>
                    Submit
                </Button>
            }

        </Form>



        </MDBMask>
    </MDBView>
            )
        }

        export default Login;